@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <ul class="nav nav-tabs">
                    <li class="active"><a style="color:black;" data-toggle="tab" href="#client">Ajout client</a></li>
                    <li><a style="color:black;" data-toggle="tab" href="#parametresadmin">Paramètres admin</a></li>
                </ul>

                <div class="panel-body">
            <div class="tab-content">
                <div id="client" class="tab-pane fade in active">
                  <form name="id_admin" class="form-horizontal" id="formregister" role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                      <label for="name" class="col-md-4 control-label">Nom</label>

                      <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                        <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                      </div>
                    </div>

                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                      <label for="login" class="col-md-4 control-label">Identifiant</label>
                      <div class="col-md-6">
                        <input id="login" type="text" class="form-control" name="login" value="{{ old('username') }}" required autofocus>

                        @if ($errors->has('username'))
                        <span class="help-block">
                          <strong>{{ $errors->first('username') }}</strong>
                        </span>
                        @endif
                      </div>
                    </div>

                     <input type="hidden" name="id_admin" id="id_admin" value="{{Auth::id()}}"></input>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                      <label for="email" class="col-md-4 control-label">Adresse mail</label>

                      <div class="col-md-6">
                        <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}">

                        @if ($errors->has('email'))
                        <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                      </div>
                    </div>

                    <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
                      <label class="col-md-4 control-label" for="tel">Teléphone</label>

                      <div class="col-md-6">
                        <input type="tel" id="tel" class="form-control" name="tel" value="{{ old('tel') }}">

                        @if ($errors->has('tel'))
                        <span class="help-block">
                          <strong>{{ $errors->first('tel') }}</strong>
                        </span>
                        @endif
                      </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" required>
                      <label class="col-md-4 control-label" for="password">Mot de passe</label>

                      <div class="col-md-6">
                        <input type="password" id="password" class="form-control" name="password">

                        @if ($errors->has('password'))
                        <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                      </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                      <label class="col-md-4 control-label" for="password">Confirmez votre mot de passe</label>

                      <div class="col-md-6">
                        <input type="password" id="password" class="form-control" name="password_confirmation" required>

                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group{{ $errors->has('admin') ? ' has-error' : '' }}">
                      <label class="col-md-4 control-label" for="admin">Droits</label>
                      <div class="col-md-6">
                          @if(auth()->user()->admin === 2)
                          {{ Form::select('admin', [ 'Client', 'Administrateur', 'SuperAdministrateur'],null, ['class' => 'form-control'])}}
                          @elseif(auth()->user()->admin === 1)
                          {{ Form::select('admin', [ 'Client'],null, ['class' => 'form-control'])}}
                          @endif
                      </div>
                    </div>
                    <div class="form-group">
          <div class="col-md-6 col-md-offset-4">

              <button  type="submit" class="btn btn-success">
                <i class="fa fa-btn fa-user" style="margin-right: 0.5em;"></i>Créer le compte
              </button>
              <a type="button" class="btn btn-default"  href="{{url('/home')}}" style= "margin-left: 8.2em;">Annuler</a>
            </div>
        </div>
    </div>
    <div id="parametresadmin" class="tab-pane fade">
        <div class="form-group">
            <label for="messages_sms">Message SMS</label>
            <textarea id="messages_sms" class="form-control" rows="8" name="messages_sms" id="messages_sms"></textarea>
        </div>
        <div class="form-group">
            <label for="messages_mail">Message mail</label>
            <textarea class="form-control" id="messages_mail" rows="8" id="messages_mail" name="messages_mail"></textarea>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
@endsection
