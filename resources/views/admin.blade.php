
  @extends('layouts.app')

  @section('content')

  <div class="container-fluid">
  <div class="onglet hidden">
    <div class="card">
    <ul  class="nav nav-tabs" style="cursor: pointer; margin-top: 3em;">
      <li class="active"><a style="color:black; font-size: 16px;" data-toggle="tab" data-target="#home">Echeances à venir</a></li>
      <li><a style="color:black; font-size: 16px;" data-toggle="tab" data-target="#menu1">Clients</a></li>
      <li><a style="color:black; font-size: 16px;" data-toggle="tab" data-target="#menu2">Véhicules</a></li>
      <li><a style="color:black; font-size: 16px;" data-toggle="tab" data-target="#menu3">Type de contrôle</a></li>
      <li><a style="color:black; font-size: 16px;" data-toggle="tab" data-target="#menu4">Interventions</a></li>
    </ul>
  </div>

    <div class="tab-content hidden">
      <div id="home" class="tab-pane fade in active">
        <div class="col-xs-3">
          <form method="GET" id="filterclient" action="{{ URL::action('HomeController@filtres') }}">
            <select class="form-control" id="clientecheances" name="clientecheances" onchange="document.getElementById('filterclient').submit();" placeholder="Rechercher un client" style="margin-top: 2em;" />
            <option>Recherche client...</option>
            @foreach($client as $clients)
            <option value="{{$clients->id}}">{{$clients->name}}</option>
            @endforeach
          </select>
        </form>
      </div>
      <div class="col-xs-2">
        <form class="" id="filtertypevehicule" action="{{ URL::action('HomeController@filtres')}}" onchange="document.getElementById('filtertypevehicule').submit();" method="get">
          <select id="type_vehicule" name="type_vehicule" class="form-control" style="margin-top: 2em;">
            <option default>Type vehicule ...</option>
            @foreach($vehicules as $vehicule)
            <option value="{{$vehicule->id}}">{{$vehicule->type_vehicule}}</option>
            @endforeach
          </select>
        </form>
      </div>
      <form method="GET" id="filterimmac" action="{{ URL::action('HomeController@filtres')}}">
        <div class="col-xs-3">
          <select  class="form-control" name="immatriculationecheances" id="immatriculationecheances"  style="margin-top: 2em;" onchange="document.getElementById('filterimmac').submit();">
            <option default>Recherche par immatriculation...</option>
            @foreach($vehicules as $vehicule)
            <option value="{{$vehicule->id}}">{{$vehicule->immatriculation}}</option>
            @endforeach
          </select>
        </div>
      </form>
      <div class="col-xs-2">
        <form method="GET" id="filtertype" action="{{ URL::action('HomeController@filtres')}}">
          <select  name="controle" id="controle" class="form-control" onchange="document.getElementById('filtertype').submit();" style="margin-top: 2em; margin-bottom: 3em;">
            <option default>Type inter ...</option>
            @foreach($type as $types)
            <option value="{{$types->id}}">{{$types->nom}}</td>
              @endforeach
            </select>
          </form>
        </div>
        <a href="{{url('/home')}}" class="btn btn-danger" style="margin-top: 2em; margin-left: 3em;">Effacer les filtres</a>
        <a style="margin-left: 3em; margin-top: 1em; width: 9.4em; margin-bottom: 2em;" href="{{ URL::to('downloadExcel/xls') }}" class="btn btn-success">Export XLS</a>
        <table class="table table-bordered" id="myTable2">
          <thead>
            <tr>
              <th>Client</th>
              <th>Type</th>
              <th>Véhicule</th>
              <th>Immatriculation</th>
              <th>Type d'inter</th>
              <th>Km reel</th>
              <th>Heure reel</th>
              <th>Prochain contrôle</th>
              <th onclick="sortTable(0)">Contrôle dans ...<a><i class="fa fa-sort fa-2x" ></i></a></th>
              <th>Action</th>
            </tr>
          </thead>
          @foreach($interventions as $intervention)
          @if($intervention->date_realisation == 0000-00-00)
          <tbody>
            <tr>
              <td>{{$intervention->recupuser['name']}}</td>
              <td>{{$intervention->recupvehicules['type_vehicule']}}</td>
              <td>{{$intervention->recupvehicules['nom']}}</td>
              <td>{{$intervention->recupvehicules['immatriculation']}}</td>
              <td>{{$intervention->recupcontrole['nom']}}</td>
              <td>{{$intervention->recupvehicules['kilometre_reel']}}</td>
              <td>{{$intervention->recupvehicules['nombre_heure']}}</td>
              @if($intervention->recupcontrole['unite'] === 'km')
              <td>{{$intervention->prochain_controle_km}} {{$intervention->recupcontrole['unite']}}</td>
              @elseif($intervention->recupcontrole['unite'] === 'heures')
              <td>{{$intervention->prochain_controle_heure}} {{$intervention->recupcontrole['unite']}}</td>
              @elseif($intervention->recupcontrole['unite'] == 'mois')
              <td>{{$intervention->prochain_controle_date}}</td>
              @endif
              @if($intervention->recupcontrole['unite'] === 'km')
              <td>{{$intervention->prochain_controle_km - $intervention->recupvehicules['kilometre_reel']}} {{$intervention->recupcontrole['unite']}} 
              @elseif($intervention->recupcontrole['unite'] === 'heures')
              <td>{{$intervention->prochain_controle_heure - $intervention->recupvehicules['nombre_heure']}} {{$intervention->recupcontrole['unite']}}</td>
              @elseif($intervention->recupcontrole['unite'] == 'mois')
              <td><?php  $now = new DateTime();
                         $dateBdd = new DateTime($intervention['prochain_controle_date']);
                         echo $now->diff($dateBdd)->days.' '.('jours'); ?></td>
              @endif
              <td><a href="{{ URL::action('InterventionsController@show', $intervention->id)}}"  class="btn btn-primary" style="width: 7em;">Suivi</a></td>
            </tr>
          </tbody> @endif
        @endforeach
      </table>
    </form>
  </div>

    <div id="menu1" class="tab-pane fade">
      <a href="{{url('/register')}}" class="btn btn-primary" data-toggle="modal"  style=" margin-top: 2em; margin-bottom: 3em; width: 10em; margin-right: 30em; ">Nouveau</a>
      <form id="filterclients" action="{{ URL::action('HomeController@filtres')}}" method="get">
        <select style="width: 20em; margin-bottom: 1em; margin-left: 35em;" class="form-control" id="client" name="client" onchange="document.getElementById('filterclients').submit();" placeholder="Rechercher un client" style="margin-top: 2em;" />
            <option>Recherche client...</option>
            @foreach($client as $users)
            <option value="{{$users->id}}">{{$users->name}}</option>
            @endforeach
          </select>
      </form>
      <a href="{{url('/home')}}" class="btn btn-danger" type="button" style="margin-left: 40.5em;margin-bottom: 2em;" >Effacer les filtres</a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Client</th>
            <th>Adresse mail</th>
            <th>Téléphone</th>
            <th>Modifier</th>
          </tr>
        </thead>
        @foreach($client as $users)
        <tbody>
          <tr>
            <td>{{$users->name}}</td>
            <td>{{$users->email}}</td>
            <td>{{$users->tel}}</td>
            <td><a href="{{ URL::action('Auth\RegisterController@show', $users->id)}}" class="btn btn-primary" style="width: 7em;">Modifier</a></td>
          </tr>
        </tbody>
        @endforeach
      </table>
    </div>
    <div id="menu2" class="tab-pane fade">
      <form id="formvehicule" action="{{route('vehicule.store')}}" method="POST">
        {{ csrf_field() }}
        <div id="vehicule" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="panel panel-default" style="margin-top: 2em; height: 45em;">
              <div class="panel-heading" style="margin-bottom: 3em;">Nouveau véhicule</div>
              <div style="margin-left: 1em;">
                <div class="row">
                  <div class="col-md-6">
                <input type="hidden" name="id_admin" id="id_admin" value="{{Auth::user()->id}}"/>
                    <div class="form-group row {{ $errors->has('id_client') ? ' has-error' : '' }}">
                      <label for="id_client" class="col-sm-3 col-form-label">Client</label>
                      <div class="col-md-8">
                        <select class="form-control" name="id_client" id="id_client">
                          <option default>Choisissez...</option>
                          @foreach($client as $clients)
                          <option value="{{$clients->id}}">{{$clients->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                        @if ($errors->has('id_client'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('id_client') }}</strong>
                              </span>
                        @endif
                    <div class="form-group row {{ $errors->has('immatriculation') ? ' has-error' : '' }}">
                      <label for="immatriculation" class="col-sm-3 col-form-label">Immatriculation</label>
                      <div class="col-md-8">
                        <input type="text" class="form-control" id="immatriculation" name="immatriculation" placeholder="Immatriculation">
                      </div>
                    </div>
                     @if ($errors->has('immatriculation'))
                              <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                  <strong>{{ $errors->first('immatriculation') }}</strong>
                              </span>
                     @endif
                    <div class="form-group row {{ $errors->has('type_vehicule') ? ' has-error' : '' }}">
                      <label for="inputPassword3" class="col-sm-3 col-form-label">Type de véhicule</label>
                      <div class="col-md-8">
                        <input class="form-control" id="type_vehicule" name="type_vehicule" placeholder="Type véhicule">
                      </div>
                    </div>
                    @if ($errors->has('type_vehicule'))
                              <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                  <strong>{{ $errors->first('type_vehicule') }}</strong>
                              </span>
                     @endif
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-3 col-form-label">Numéro de série</label>
                      <div class="col-md-8">
                        <input type="text" name="num_serie" id="num_serie" class="form-control" placeholder="Numéro de série">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-3 col-form-label">Numéro de moteur</label>
                      <div class="col-md-8">
                        <input type="text" name="num_moteur" id="num_moteur" class="form-control" placeholder="Numéro de moteur">
                      </div>
                    </div>

                    <div class="form-group">
                      <div style="margin-top: 7.5em; width: 28em;">
                        <label for="inputPassword3">Contrôle à effectuer</label>
                        <select class="form-control" id="id_controle" name="id_controle" style="margin-bottom:2em;">
                          <option default>Controle à effectuer...</option>
                          @foreach($type as $types)
                          <option  value="{{$types->id}}">{{$types->nom}}  | {{$types->periodicite}} {{$types->unite}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <button  style="width: 20em; margin-left: 3em; margin-top:0.5em;" type="submit" class="btn btn-success">
                        <i class="fa fa-car" aria-hidden="true" style="margin-right: 0.5em;"></i>Valider
                      </button>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <!-- <div class="checkbox" style="margin-left: 24em;">
                    <label><input type="checkbox" value="">Actif</label>
                  </div> -->
                  <div class="form-group row  {{ $errors->has('type_vehicule') ? ' has-error' : '' }}">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Nom Vehicule</label>
                    <div class="col-md-8">
                      <input type="text" name="nom" id="nom" class="form-control" placeholder="Nom véhicule">
                    </div>
                  </div>
                  @if ($errors->has('nom'))
                              <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                  <strong>{{ $errors->first('nom') }}</strong>
                              </span>
                  @endif
                  <div class="form-group row {{ $errors->has('type_vehicule') ? ' has-error' : '' }}">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Km réel</label>
                    <div class="col-md-8">
                      <input type="text" name="kilometre_reel" id="kilometre_reel" class="form-control" placeholder="Km reel">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Nb Heure</label>
                    <div class="col-md-8">
                      <input type="text" name="nombre_heure" id="nombre_heure" class="form-control" placeholder="Nombre d'heures">
                    </div>
                  </div>
                  <div class="form-group row {{ $errors->has('description') ? ' has-error' : '' }}">
                    <div style="margin-left: 1.1em;margin-top: 3.8em;">
                      <label for="inputPassword3">Zone libre: </label>
                      <textarea type="text" rows="5" name="description" id="description" class="form-control" placeholder="Zone libre" style="width:27.8em;"></textarea>
                    </div>
                  </div>
                </div>
                @if ($errors->has('description'))
                              <span class="help-block" style="color: DarkRed; margin-left: 12em;">
                                  <strong>{{ $errors->first('description') }}</strong>
                              </span>
                @endif
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group row {{ $errors->has('dernier_releve') ? ' has-error' : '' }}">
                      <div style="margin-left: 1.1em;">
                        <label for="dernier_controle">Dernier controle</label>
                        <input type="text" name="dernier_controle" id="dernier_controle" class="form-control" style="width: 13em;"></input>
                      </div>
                    </div>
                  </div>
                  @if ($errors->has('dernier_releve'))
                              <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                  <strong>{{ $errors->first('dernier_releve') }}</strong>
                              </span>
                  @endif
                  <div class="col-md-3">
                    <div class="form-group row">
                      <div style="margin-left: 1.1em;">
                        <label for="inputPassword3">Prochain contrôle</label>
                        <input disabled="disabled" name="prochain_controle" id="prochain_controle" class="form-control" style="width: 11em;"></input>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <a href="{{url('/home')}}" type="button" class="btn btn-default" data-dismiss="modal" style="width: 20em; margin-left: 7em; margin-top: 1.5em;">Annuler</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    <a id="nouveauvehicule" class="btn btn-primary" data-toggle="modal"  style=" margin-bottom: 3em; margin-top: 2em; margin-left: 1em; width: 10em;">Nouveau</a>
    <div class="container">
    <div class="alert alert-success alert-dismissible hidden">
              Le vehicule à bien été ajouté !
    </div> 
  </div>
    <div class="col-md-2" >
   <form id="filterclientvehicules" action="{{ URL::action('HomeController@filtres')}}" method="get">
        <select class="form-control" style="width: 12em; margin-right: 1em;"
        name="clientvehicule" id="clientvehicule" onchange="document.getElementById('filterclientvehicules').submit();">
          <option default>Filtrer par client</option>
          @foreach($client as $clients)
          <option value="{{$clients->id}}">{{$clients->name}}</option>
          @endforeach
        </select>
      </div>
    </form>
    <a style=" width: 9.4em; margin-left: 47em; margin-bottom: 1em;" href="{{ URL::to('downloadExcelvehicule/xls') }}" class="btn btn-success">Export XLS</a>
    <div class="col-md-2">
      <form id="filterimmacvehicules" action="{{ URL::action('HomeController@filtres')}}" method="get">
        <select class="form-control" name="immatriculationvehicule" id="immatriculationvehicule" style="margin-left: 15.6em; width: 15.2em; margin-bottom: 3em;" onchange="document.getElementById('filterimmacvehicules').submit();">
          <option default value="">Filtrer par immatriculation</option>
          @foreach($vehicules as $vehicule)
          <option value="{{$vehicule->id}}">{{$vehicule->immatriculation}}</option>
          @endforeach
        </select>
      </div>
    <a href="{{url('/home')}}" class="btn btn-danger" style="margin-left: 47em; margin-bottom: 2em;" >Effacer les filtres</a>
    </form>
    <table class="table table-bordered tablevehicule" >
      <thead>
        <tr>
          <th>Client</th>
          <th>Type</th>
          <th>Véhicule</th>
          <th>Immatriculation</th>
          <th>Km reel</th>
          <th>Nombre heure</th>
          <th>Edition</th>
        </tr>
      </thead>
      @foreach($vehicules as $vehicule)
      <tbody>
        <tr>
          <td>{{$vehicule->user['name']}}</td>
          <td>{{$vehicule->type_vehicule}}</td>
          <td>{{$vehicule->nom}}</td>
          <td>{{$vehicule->immatriculation}}</td>
          <td>{{$vehicule->kilometre_reel}}</td>
          <td>{{$vehicule->nombre_heure}}</td>
          <td><a href="{{ URL::action('VehiculeController@showmodificationvehicule', $vehicule->id)}}" class="btn btn-primary"><i style="margin-right: 0.5em;" class="fa fa-pencil-square-o" aria-hidden="true"></i>Edition véhicule</a></td>
        </tr>
       </tbody>
      @endforeach
    </table>
  </div>
  <div id="menu3" class="tab-pane fade">
    <form id="formtype" action="{{route('typecontrole.store')}}" method="POST">
      {{ csrf_field() }}
      <div id="type" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="panel panel-default" style="margin-top: 2em; height: 35em;">
            <div class="panel-heading" style="margin-bottom: 3em;">Nouveau type de contrôle</div>
            <div style="margin-left: 1em;">
              <input type="hidden" name="id_admin" id="id_admin" value="{{Auth::id()}}">
              <div class="form-group row">
                <label for="code" class="col-sm-3 col-form-label">Code</label>
                <div class="col-md-8">
                  <input class="form-control" type="text" name="code" id="code" placeholder="Code client">
                </div>
              </div>
              <div class="form-group row">
                <label for="nom" class="col-sm-3 col-form-label">Nom</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom du type de controle">
                </div>
              </div>

              <div class="form-group row">
                <label for="periodicite" class="col-sm-3 col-form-label">Periodicité</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="periodicite" id="periodicite" placeholder="Periodicité">
                </div>
              </div>
              <div class="form-group row">
                <label for="unite" class="col-sm-3 col-form-label">Unité</label>
                <div class="col-md-8">
                  <select class="form-control" name="unite" id="unite">
                    <option value="km">Km</option>
                    <option value="heures">Heures</option>
                    <option value="mois">Mois</option>
                  </select>
                </div>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Envoyez par mail</label>
              </div>
              <div class="form-group row">
                <label for="alerte" class="col-sm-3 col-form-label">Nombres de jours avant alerte</label>
                <div class="col-md-8">
                  <input type="text" name="alerte" id="alerte" class="form-control" placeholder="Nombres de jours avant alerte">
                </div>
              </div>
              <div class="form-group">
                <button  style="width: 20em; margin-left: 3em; margin-top:1.3em;" type="submit" class="btn btn-success">Valider</button>
                <a href="{{url('/home')}}" class="btn btn-default" data-dismiss="modal" style="width: 20em; margin-left: 15em; margin-top: 1.2em;">Annuler</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    <button class="btn btn-primary" data-toggle="modal" id="nouveautype" style="margin-top:2em; margin-bottom: 2em; width: 10em;">Nouveau</button>
    <div class="container">
    <div class="alert alert-success alert-dismissible hidden">
              Le type de contrôle à bien été ajouté !
    </div> 
  </div>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Code</th>
          <th>Nom</th>
          <th>Periodicité</th>
          <th>Unité</th>
        </tr>
      </thead>


      @foreach ($type as $types)

      <tbody>
        <tr>
          <td>{{$types->code}}</td>
          <td>{{$types->nom}}</td>
          <td>{{$types->periodicite}}</td>
          <td>{{$types->unite}}</td>
        </tr>
      </tbody>


      @endforeach

    </table>
  </div>
  <div id="menu4" class="tab-pane fade">
  <form method="GET" id="filterclientinter" action="{{ URL::action('HomeController@filtres')}}">
    <div class="col-xs-3">
    <select class="form-control" id="clientinter" name="clientinter" onchange="document.getElementById('filterclientinter').submit();" style="margin-top: 2em;">
      <option default>Recherche client...</option>
      @foreach($client as $clients)
      <option value="{{$clients->id}}">{{$clients->name}}</option>
      @endforeach
    </select>
    </div>
  </form>

  <form method="GET" id="filtertypeinter" action="{{ URL::action('HomeController@filtres')}}">
    <div class="col-xs-2" style="margin-right: 13em;">
      <select class="form-control" style="margin-top: 2em;" name="typeinter" id="typeinter" onchange="document.getElementById('filtertypeinter').submit();">
        <option default>Type vehicule...</option>
        @foreach($vehicules as $vehicule)
        <option value="{{$vehicule->id}}">{{$vehicule->type_vehicule}}</option>
        @endforeach
      </select>
    </div>
  </form>
    <div class="col-xs-3">
      <form method="GET" id="filterimmacinter" action="{{ URL::action('HomeController@filtres')}}">
      <select class="form-control" style="margin-top: 2em;" name="immacinter" id="immacinter" onchange="document.getElementById('filterimmacinter').submit();">
        <option default>Recherche par immatriculation...</option>
        @foreach($vehicules as $vehicule)
        <option value="{{$vehicule->id}}">{{$vehicule->immatriculation}}</option>
        @endforeach
      </select>
    </div>
  </form>
      <form method="GET" id="filtercontroleinter" action="{{ URL::action('HomeController@filtres')}}">
    <div class="col-xs-2">
      <select class="form-control" onchange="document.getElementById('filtercontroleinter').submit()"; name="controleinter" name="controleinter" style="margin-top: 2em; margin-bottom: 3em;">
        <option default>Type Inter...</option>
        @foreach($type as $types)
        <option value="{{$types->id}}">{{$types->nom}}</option>
        @endforeach
      </select>
    </div>
  </form>
  <a href="{{url('/home')}}" class="btn btn-danger" style="margin-left: 40em; margin-bottom: 3em;">Effacer les filtres</a>
    <table class="table table-bordered" >
      <thead>
        <tr>
          <th>Date de realisation</th>
          <th>Client</th>
          <th>Type</th>
          <th>Véhicule</th>
          <th>Immatriculation</th>
          <th>Type d'inter</th>
          <th>Contrôle effectué à ...</th>
          <th>Description</th>
        </tr>
      </thead>



      @foreach($interventions as $key => $intervention)
      @if($intervention->date_realisation > 0000-00-00)
      <tbody>
        <tr>
          <td>{{$intervention->date_realisation}}</td>
          <td>{{$intervention->recupuser['name']}}</td>
          <td>{{$intervention->recupvehicules['type_vehicule']}}</td>
          <td>{{$intervention->recupvehicules['nom']}}</td>
          <td>{{$intervention->recupvehicules['immatriculation']}}</td>
          <td>{{$intervention->recupcontrole['nom']}}</td>
          @if($intervention->recupcontrole['unite'] === 'km')
          <td>{{$intervention->prochain_controle_km}} {{$intervention->recupcontrole['unite']}}</td>
          @elseif($intervention->recupcontrole['unite'] === 'heures')
          <td>{{$intervention->prochain_controle_heure}} {{$intervention->recupcontrole['unite']}}</td>
          @elseif($intervention->recupcontrole['unite'] == 'mois')
          <td>{{$intervention->prochain_controle_date}}</td>
          @endif
          <td>{{$intervention->description}}</td>
        </tr>
      </tbody>
      @endif
      @endforeach
      </table>
  </div>
  </div>
  </div>
  </div>
  </div>
  @include('layouts.errors')
  @endsection


  @section('script')
    <script src={{asset('js/main.js')}} type="text/javascript"></script>
  @endsection
