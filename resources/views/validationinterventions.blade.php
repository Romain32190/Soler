@extends('layouts.app')

@section('content')
<div class="container">
@foreach($interventions as $intervention)
<form id="formintervention" action="{{ URL::action('InterventionsController@store', $intervention->id)}}" method="post">
  {{ csrf_field() }}
@endforeach
      <div class="panel panel-default" style="margin-top: 2em; height: 40em;">
        <div class="panel-heading" style="margin-bottom: 3em;">Interventions</div>
        <div style="margin-left: 1em;">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label" style="margin-left: 1em;">Client :</label>
                <div class="col-md-8">

                 <input type="hidden" name="id_admin" id="id_admin" value="{{Auth::user()->id}}"/>

                  @foreach($interventions as $intervention)
                    <input type="hidden" name="id_client" id="id_client" value="{{$intervention->recupuser['id']}}">
                  @endforeach

                  @foreach($interventions as $intervention)
                    <input type="hidden" name="id_vehicule" id="id_vehicule" value="{{$intervention->recupvehicules['id']}}">
                  @endforeach

                  @foreach($interventions as $intervention)
                    <input type="hidden" name="id_controle" id="id_controle" value="{{$intervention->recupcontrole['id']}}">
                  @endforeach

                  @foreach($interventions as $intervention)
                  <label>{{$intervention->recupuser['name']}}</label>
                  @endforeach
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label" style="margin-left: 1em;">Immatriculation :</label>
                <div class="col-md-8">
                  @foreach ($interventions as $intervention)
                  <label  value="{{$intervention->id}}">{{$intervention->recupvehicules['immatriculation']}}</label>
                  @endforeach
                  </div>
                </div>
                <div class="form-group">
                  <div style="margin-top: 3.3em; width: 28em;">
                   <label for="inputPassword3" style="margin-right: 4em;" class="col-sm-5 col-form-label">Contrôle à effectuer</label>
                    <select name="id_controle" id="id_controle" class="form-control" disabled style="margin-left: 1em;">
                      @foreach($interventions as $intervention)
                      <option  value="{{$intervention->recupcontrole['nom']}}">{{$intervention->recupcontrole['nom']}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <button style="width: 20em; margin-left: 5.5em; margin-top: 10em;" class="btn btn-primary">Enregistrer</button>
                  <button style="width: 20em; margin-left: 5.5em; margin-top: 2em; margin-bottom:5em;" type="submit" class="btn btn-success">
                    Valider intervention
                  </button>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row{{ $errors->has('kilometre_reel') ? ' has-error' : '' }}">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Dernier contrôle</label>
                <div class="col-md-8">
                  @if($intervention->recupcontrole['unite'] === 'km')
                  <input readonly class="form-control" value="{{$intervention->dernier_controle_km}}" type="text" name="dernier_controle" placeholder="Kilometre réel">
                  @elseif($intervention->recupcontrole['unite'] === 'heures')
                  <input readonly class="form-control" value="{{$intervention->dernier_controle_heure}}" type="text" name="dernier_controle" placeholder="Kilometre réel">
                  @elseif($intervention->recupcontrole['unite'] == 'mois')
                  <input readonly class="form-control" value="{{$intervention->dernier_controle_date}}" type="text" name="dernier_controle" placeholder="Kilometre réel">
                  @endif
                </div>
              </div>
              <div class="form-group row{{ $errors->has('kilometre_reel') ? ' has-error' : '' }}">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Prochain contrôle</label>
                <div class="col-md-8">
                @if($intervention->recupcontrole['unite'] === 'km')
                  <input readonly class="form-control" value="{{$intervention->prochain_controle_km}}" type="text" name="prochain_controle" placeholder="Kilometre réel">
                @elseif($intervention->recupcontrole['unite'] === 'heures')
                <input readonly class="form-control" value="{{$intervention->prochain_controle_heure}}" type="text" name="prochain_controle" placeholder="Kilometre réel">
                @elseif($intervention->recupcontrole['unite'] === 'mois')
                <input readonly class="form-control" value="{{$intervention->prochain_controle_date}}" type="text" name="prochain_controle" placeholder="Kilometre réel">
                  @endif
                </div>
              </div>
              <div class="form-group row{{ $errors->has('kilometre_reel') ? ' has-error' : '' }}">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Kilometre rêel</label>
                <div class="col-md-8">
                  <input class="form-control" type="text" name="kilometre_reel_inter" id="kilometre_reel" placeholder="Kilometre réel">
                </div>
              </div>
              <div class="form-group row{{ $errors->has('kilometre_reel') ? ' has-error' : '' }}">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Nombre heure</label>
                <div class="col-md-8">
                  <input class="form-control" type="text" name="nombre_heure_inter" id="nombre_heure" placeholder="Nombre heure">
                </div>
              </div>
            <div class="form-group row {{ $errors->has('kilometre_reel') ? ' has-error' : '' }}">
              <div style="margin-left: 1.2em;margin-top: 1em;">
                <label for="inputPassword3">Description: </label>
                <textarea type="text" rows="4" name="description" id="description" class="form-control" placeholder="Zone libre" style="width:35.5em;"></textarea>
              </div>
            </div>
            @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong style="color: DarkRed; margin-left: 10.3em;">{{ $errors->first('description') }}</strong>
                                    </span>
              @endif
            </div>
            <div class="form-group">
              <a href="{{url('/home')}}" class="btn btn-default" data-dismiss="modal" style="width: 20em; margin-left: 9em; margin-top: 1.5em;">Annuler</a>
          </div>
      </div>
  </form>
@endsection
