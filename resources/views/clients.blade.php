@extends('layouts.app')

@section('content')

<div class="container-fluid">
<div class="onglet hidden">
<div class="card">
  <ul class="nav nav-tabs">
    <li class="active"><a style="color:black; font-size: 16px;" data-toggle="tab" data-target="#home">Echeances à venir</a></li>
    <li><a style="color:black; font-size: 16px;" data-toggle="tab" data-target="#menu2">Véhicules</a></li>
    <li><a style="color:black; font-size: 16px;" data-toggle="tab" data-target="#menu4">Interventions</a></li>
  </ul>
</div>
  <div class="tab-content hidden">
    <div id="home" class="tab-pane fade in active">
      <div class="col-xs-2" style="margin-left: 13em;">
        <form id="filterclienttype" action="{{ URL::action('ClientController@clientfiltres')}}" method="get">
          <select class="form-control" name="type_vehicule" id="type_vehicule" style="margin-top: 2em;" onchange="document.getElementById('filterclienttype').submit();">
            <option default>Type vehicule ...</option>
            @foreach(auth()->user()->vehicules as $vehicule)
              <option value="{{$vehicule->type_vehicule}}">{{$vehicule->type_vehicule}}
              </option>
            @endforeach
          </select>
        </form>
      </div>
      <div class="col-xs-3">
        <form id="filterclientimmac" action="{{ URL::action('ClientController@clientfiltres')}}" method="get">
          <select id="immatriculationclient" name="immatriculationclient" onchange="document.getElementById('filterclientimmac').submit();" class="form-control" style="margin-top: 2em; margin-bottom: 3em;">
          <option default>Recherche par immatriculation ...</option>
          @foreach($vehicules as $vehicule)
            <option value="{{$vehicule->id}}">{{$vehicule->immatriculation}}</td>
          @endforeach
          </select>
        </form>
      </div>
      <div class="col-xs-2">
        <select id="table_format" class="form-control" style="margin-top: 2em; margin-bottom: 3em;">
          <option default>Type inter ...</option>
          @foreach($type as $types)
            <option value="">{{$types->nom}}</td>
          @endforeach
          </select>
        </div>
        <a href="{{url('/tableaudebord')}}" class="btn btn-danger" style="margin-top: 2em; margin-left: 1em;">Effacer les filtres</a>
        <table  class="table table-bordered">
          <thead>
            <tr>
              <th>Client</th>
              <th>Type</th>
              <th>Véhicule</th>
              <th>Immatriculation</th>
              <th>Type d'inter</th>
              <th>Kilomètre réel</th>
              <th>Heure réel</th>
              <th>Prochain contrôle</th>
              <th>Contrôle dans ...</th>
              <th>Action</th>
            </tr>
          </thead>
          @foreach(auth()->user()->userinter as $intervention)
          @if($intervention->date_realisation == 0000-00-00)
          <tbody>
            <tr>
               <td>{{$intervention->recupuser['name']}}</td>
            <td>{{$intervention->recupvehicules['type_vehicule']}}</td>
            <td>{{$intervention->recupvehicules['nom']}}</td>
            <td>{{$intervention->recupvehicules['immatriculation']}}</td>
            <td>{{$intervention->recupcontrole['nom']}}</td>
            <td>{{$intervention->recupvehicules['kilometre_reel']}}</td>
            <td>{{$intervention->recupvehicules['nombre_heure']}}</td>
            @if($intervention->recupcontrole['unite'] === 'km')
            <td>{{$intervention->prochain_controle_km}} {{$intervention->recupcontrole['unite']}}</td>
            @elseif($intervention->recupcontrole['unite'] === 'heures')
            <td>{{$intervention->prochain_controle_heure}} {{$intervention->recupcontrole['unite']}}</td>
            @elseif($intervention->recupcontrole['unite'] == 'mois')
            <td>{{$intervention->prochain_controle_date}}</td>
            @endif
            @if($intervention->recupcontrole['unite'] === 'km')
            <td>{{$intervention->prochain_controle_km - $intervention->recupvehicules['kilometre_reel']}} {{$intervention->recupcontrole['unite']}} 
            @elseif($intervention->recupcontrole['unite'] === 'heures')
            <td>{{$intervention->prochain_controle_heure - $intervention->recupvehicules['nombre_heure']}} {{$intervention->recupcontrole['unite']}}</td>
            @elseif($intervention->recupcontrole['unite'] == 'mois')
            <td><?php  $now = new DateTime();
                       $dateBdd = new DateTime($intervention['prochain_controle_date']);
                       echo $now->diff($dateBdd)->days.' '.('jours'); ?></td>
            @endif
              <td><button class="btn btn-default" type="button">Différer</button></td>
            </tr>
          </tbody>
          @endif
          @endforeach
        </table>
      </div>

      <div id="menu4" class="tab-pane fade">
        <div class="col-xs-2" style="margin-left: 4em;">
          <select class="form-control" style="margin-top: 2em; margin-left: 5em;">

          </select>
        </div>
        <div class="col-xs-3">
          <input type="text" class="form-control" placeholder="Rechercher par plaque d'immatriculation" style="margin-top: 2em; margin-left: 15em;"/>
        </div>
        <div class="col-xs-2">
          <select class="form-control" style="margin-top: 2em; margin-bottom: 3em; margin-left: 15em;">

          </select>
        </div>
        <table class="table table-bordered" style="margin-top:6em;">
          <thead>
            <tr>
              <th>Date de realisation</th>
              <th>Client</th>
              <th>Type</th>
              <th>Véhicule</th>
              <th>Immatriculation</th>
              <th>Type d'inter</th>
              <th>Contrôle effectué à ...</th>
              <th>Description</th>
            </tr>
          </thead>
  @foreach(auth()->user()->userinter as $intervention)
  @if($intervention->date_realisation > 0000-00-00)
    <tbody>
      <tr>
        <td>{{$intervention->date_realisation}}</td>
        <td>{{$intervention->recupuser['name']}}</td>
        <td>{{$intervention->recupvehicules['type_vehicule']}}</td>
        <td>{{$intervention->recupvehicules['nom']}}</td>
        <td>{{$intervention->recupvehicules['immatriculation']}}</td>
        <td>{{$intervention->recupcontrole['nom']}}</td>
        @if($intervention->recupcontrole['unite'] === 'km')
        <td>{{$intervention->prochain_controle_km}} {{$intervention->recupcontrole['unite']}}</td>
        @elseif($intervention->recupcontrole['unite'] === 'heures')
        <td>{{$intervention->prochain_controle_heure}} {{$intervention->recupcontrole['unite']}}</td>
        @elseif($intervention->recupcontrole['unite'] == 'mois')
        <td>{{$intervention->prochain_controle_date}}</td>
        @endif
        <td>{{$intervention->description}}</td>
      </tr>
    </tbody>
    @endif
@endforeach
        </table>
      </div>
      <div id="menu2" class="tab-pane fade">
        <div class="col-xs-2" style="margin-top: 3em;">
          <select class="form-control" style=" margin-left : 15em;" name="id_client" id="id_client">
            <option default >Type vehicule...</option>
            @foreach(auth()->user()->vehicules as $vehicule)
              <option value="{{$vehicule->user['id']}}">{{$vehicule->user['name']}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-xs-3" style="margin-top: 3em;">
          <select class="form-control" style="margin-bottom: 2em; margin-left: 26em;">
            <option default value="">Filtrer par immatriculation</option>
            @foreach(auth()->user()->vehicules as $vehicule)
              <option value="">{{$vehicule->immatriculation}}</option>
            @endforeach
          </select>
        </div>
        <a href="{{url('/tableaudebord')}}" class="btn btn-danger" style="margin-left: 27em; margin-top: 3em;">Effacer les filtres</a>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Client</th>
              <th>Type</th>
              <th>Véhicule</th>
              <th>Immatriculation</th>
              <th>Km réel</th>
              <th>Nombre heure</th>
            </tr>
          </thead>
          @foreach(auth()->user()->vehicules as $vehicule)
          <tbody>
            <tr>
              <td>{{$vehicule->user['name']}}</td>
              <td>{{$vehicule->type_vehicule}}</td>
              <td>{{$vehicule->nom}}</td>
              <td>{{$vehicule->immatriculation}}</td>
              <td>{{$vehicule->kilometre_reel}}</td>
              <td>{{$vehicule->nombre_heure}}</td>
            </tr>
          </tbody>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>
</div>

  @endsection
