@extends('layouts.app')

@section('content')

<h2 style="text-align: center; margin-top: 2em; font-weight: bold;">Edition véhicule</h2>

<div class="container">
 <form class="form-horizontal" role="form" method="POST" action="home/modificationvehicule{{ $vehicules[0]->id }}">
 {{ csrf_field() }}
<div class="panel panel-default" style="margin-top: 2em; height: 100%;">
            <div class="panel-heading" style="margin-bottom: 3em;">Modification véhicule<a class="btn btn-warning" style="margin-left: 55em;">Desactiver le véhicule</a></div>
            <div style="margin-left: 1em;">
              <div class="row">
                <div class="col-md-6">
              <input type="hidden" name="id_admin" id="id_admin" value="{{Auth::user()->id}}"/>
              @foreach($vehicules as $vehicule)
              <input type="hidden" name="id_vehicule" id="id_vehicule" value="{{$vehicule->id}}">
              @endforeach
              @foreach($vehicules as $vehicule)
                <input type="hidden" name="id_client" id="id_client" value="{{$vehicule->id_client}}">
                @endforeach
                  <div class="form-group row {{ $errors->has('id_client') ? ' has-error' : '' }}">
                    <label for="id_client" class="col-sm-3 col-form-label">Client</label>
                    <div class="col-md-8">
                      <select class="form-control" name="id_client" id="id_client">
                        @foreach($vehicules as $vehicule)
                        <option value="{{$vehicule->id}}">{{$vehicule->user['name']}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                      @if ($errors->has('id_client'))
                            <span class="help-block">
                                <strong>{{ $errors->first('id_client') }}</strong>
                            </span>
                      @endif
                  <div class="form-group row {{ $errors->has('immatriculation') ? ' has-error' : '' }}">
                    <label for="immatriculation" class="col-sm-3 col-form-label">Immatriculation</label>
                    <div class="col-md-8">
                      <input type="text" value="{{$vehicule->immatriculation}}" class="form-control" id="immatriculation" name="immatriculation" placeholder="Immatriculation">
                    </div>
                  </div>
                   @if ($errors->has('immatriculation'))
                            <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                <strong>{{ $errors->first('immatriculation') }}</strong>
                            </span>
                   @endif
                  <div class="form-group row {{ $errors->has('type_vehicule') ? ' has-error' : '' }}">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Type de véhicule</label>
                    <div class="col-md-8">
                      <input class="form-control" value="{{$vehicule->type_vehicule}}" id="type_vehicule" name="type_vehicule" placeholder="Type véhicule">
                    </div>
                  </div>
                  @if ($errors->has('type_vehicule'))
                            <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                <strong>{{ $errors->first('type_vehicule') }}</strong>
                            </span>
                   @endif
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Numéro de série</label>
                    <div class="col-md-8">
                      <input type="text" name="num_serie" value="{{$vehicule->num_serie}}" id="num_serie" class="form-control" placeholder="Numéro de série">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Numéro de moteur</label>
                    <div class="col-md-8">
                      <input type="text" name="num_moteur" id="num_moteur" class="form-control" value="{{$vehicule->num_moteur}}" placeholder="Numéro de moteur">
                    </div>
                  </div>

                 <i id="nouveaucontrole" class="fa fa-plus-circle fa-2x" aria-hidden="true" style="margin-top: 1em;"><span style="font-size: 19.5px; margin-left: 0.5em;">Ajouter un nouveau contrôle pour ce véhicule</i>
                  <div style="margin-top: 4em; width: 28em;">
                      <label for="inputPassword3">Contrôle effectuer</label>
                    @foreach($controle as $controles)
                      <select  disabled="" class="form-control" style="margin-bottom:2em;">
                        <option value="{{$controles->recuptypeinter['id']}}">{{$controles->recuptypeinter['nom']}}</option>
                      </select>
                    @endforeach
                    </div>
                  <div class="form-group">
                    <button  style="width: 20em; margin-left: 10em; margin-top:2.3em;" type="submit" class="btn btn-success">
                      <i class="fa fa-car" aria-hidden="true" style="margin-right: 0.5em;"></i>Valider
                    </button>
                  </div>
                </div>
                <div class="col-md-6">
                  <!-- <div class="checkbox" style="margin-left: 24em;">
                  <label><input type="checkbox" value="">Actif</label>
                </div> -->
                <div class="form-group row  {{ $errors->has('type_vehicule') ? ' has-error' : '' }}">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Nom Vehicule</label>
                  <div class="col-md-8">
                    <input type="text" name="nom" id="nom" value="{{$vehicule->nom}}" class="form-control" placeholder="Nom véhicule">
                  </div>
                </div>
                @if ($errors->has('nom'))
                            <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                <strong>{{ $errors->first('nom') }}</strong>
                            </span>
                @endif
                <div class="form-group row {{ $errors->has('type_vehicule') ? ' has-error' : '' }}">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Km réel</label>
                  <div class="col-md-8">
                    <input type="text" value="{{$vehicule->kilometre_reel}}" name="kilometre_reel" id="kilometre_reel" class="form-control" placeholder="Km reel">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Nb Heure</label>
                  <div class="col-md-8">
                    <input type="text" name="nombre_heure" id="nombre_heure" value="{{$vehicule->nombre_heure}}" class="form-control" placeholder="Nombre d'heures">
                  </div>
                </div>
                <div class="form-group row {{ $errors->has('description') ? ' has-error' : '' }}">
                  <div style="margin-left: 1.1em;margin-top: 0.8em;">
                    <label for="inputPassword3">Zone libre: </label>
                    <textarea type="text" rows="5" name="description" id="description" class="form-control" placeholder="Zone libre" style="width:35.8em;">{{$vehicule->description}}</textarea>
                  </div>
                </div>
              </div>
              @if ($errors->has('description'))
                            <span class="help-block" style="color: DarkRed; margin-left: 12em;">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
              @endif

          </form>
              <div class="row">
                <div class="col-md-2">
                  <div class="form-group row {{ $errors->has('dernier_releve') ? ' has-error' : '' }}">
                    <div style="margin-right: 3em; margin-top: 4em;">
                      <label for="dernier_controle">Dernier contrôle</label>
                      @foreach($controle as $controles)

                      <input type="text" class="form-control" value="{{$controles->dernier_controle}}" style="width: 11em; margin-bottom: 1.8em;" disabled=""></input>
                      @endforeach
                    </div>
                  </div>
                </div>
                @if ($errors->has('dernier_releve'))
                            <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                <strong>{{ $errors->first('dernier_releve') }}</strong>
                            </span>
                @endif
                <div class="col-md-2">
                  <div class="form-group row">
                    <div style=" margin-top: 4em;">
                      <label for="prochain_controle">Prochain contrôle</label>
                      @foreach($controle as $controles)
                      <form action="/suppressioncontrole{{$controles->id}}" method="post">
                      {{ method_field('DELETE') }}
                      {{csrf_field()}}
                      <button type="submit" class="btn btn-danger" style="margin-left: 13em;">Supprimer le contrôle</button>
                      <input type="text" class="form-control" name="prochain_controle" value="{{$controles->prochain_controle}}" id="prochain_controle" style="width: 11em; margin-top:-2.6em; margin-bottom: 2em;"  disabled=""></input>
                      @endforeach 
                    </div>
                  </div>
                </div> 
              </form>               
                <div class="form-group row">
                  <a href="{{url('/home')}}" type="button" class="btn btn-default" data-dismiss="modal" style="width: 20em; margin-left: 7em; margin-top: 1em;">Annuler</a>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

 <form id="formcontrole" class="form-horizontal" method="POST" action="/home/nouveaucontrole{{$vehicule->id}}">
 {{ csrf_field() }}
  <div id="controle" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="panel panel-default" style="height: 30em;">
        <div class="panel-heading">Nouveau controle véhicule</div>
          <div style="margin-left: 1em;">
            <div class="col-md-6">
              <div class="form-group">
                @foreach($vehicules as $vehicule)
                <input type="hidden" name="id_vehicule" id="id_vehicule" value="{{$vehicule->id}}">
                @endforeach
                <input type="hidden" name="id_admin" id="id_admin" value="{{Auth::id()}}">
                @foreach($vehicules as $vehicule)
                <input type="hidden" name="id_client" id="id_client" value="{{$vehicule->id_client}}">
                @endforeach
                    <div style="margin-top: 3.8em; width: 28em;">
                      <label for="id_controle">Contrôle effectuer</label>
                      <select name="id_controle" id="id_controle"  class="form-control" style="margin-bottom:2em;">
                    @foreach($type as $types)
                    <option value="{{$types->id}}">{{$types->nom}} | {{$types->periodicite}} {{$types->unite}}</option>
                    @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                          <button  style="width: 20em; margin-left: 3em; margin-top:7em;" type="submit" class="btn btn-primary">
                            <i class="fa fa-car" style="margin-right: 0.5em;"></i>Enregistrer
                          </button>
                  </div>
                </div>
                <div class="row">
                <div class="col-md-3">
                  <div class="form-group row {{ $errors->has('dernier_releve') ? ' has-error' : '' }}">
                    <div style="margin-left: 1.1em; margin-top: 3.8em;">
                      <label for="dernier_controle">Dernier contrôle</label>
                      <input name="dernier_controle"  id="dernier_controle" type="text" class="form-control"></input>
                    </div>
                  </div>
                </div>
                @if ($errors->has('dernier_releve'))
                            <span class="help-block" style="color: DarkRed; margin-left: 8.2em;">
                                <strong>{{ $errors->first('dernier_releve') }}</strong>
                            </span>
                @endif
                <div class="col-md-3">
                  <div class="form-group row">
                    <div style="margin-left: 1.1em; margin-top: 3.8em;">
                      <label for="prochain_controle">Prochain contrôle</label>
                      <input type="text" class="form-control" readonly="" name="prochain_controle" id="prochain_controle" style="width: 11em;"></input>
                    </div>
                  </div>
              </div>
            <a type="button" class="btn btn-default" data-dismiss="modal" style="width: 20em; margin-left: 7em; margin-top: 9.1em;">Annuler</a>
            </div>
          </div>
        </div>
    </div> 
  </div>
</form>

@endsection
