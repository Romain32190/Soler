@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#client">Modification Client/Admin</a></li>
          <li><a data-toggle="tab" href="#parametresadmin">Paramètres admin</a></li>
          <button class="btn btn-warning" style="margin-left: 17.37em; height: 3.2em;">Désactiver le client</button>
        </ul>


        <div class="panel-body">
          <div class="tab-content">
            <div id="client" class="tab-pane fade in active">
              <form class="form-horizontal" role="form" method="POST" action="home/modifregister{{ $users[0]->id }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Nom</label>

                  @foreach($users as $client)
                  <input type="hidden" name="id_admin" id="id_admin" value="{{$client->id_admin}}">
                  @endforeach

                  <div class="col-md-6">
                    @foreach($users as $client)
                    <input id="name" type="text" class="form-control" name="name" value="{{$client->name}}" required autofocus>
                    @endforeach

                    @if ($errors->has('name'))
                    <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
                  <label class="col-md-4 control-label">Teléphone</label>

                  <div class="col-md-6">
                    @foreach($users as $client)
                    <input type="tel" class="form-control" name="tel" value="{{$client->tel}}">
                    @endforeach

                    @if ($errors->has('tel'))
                    <span class="help-block">
                      <strong>{{ $errors->first('tel') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" required>
                  <label class="col-md-4 control-label">Mot de passe</label>

                  <div class="col-md-6">
                    @foreach($users as $client)
                    <input type="password" class="form-control" name="password" value="{{$client->password}}">
                    @endforeach

                    @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label class="col-md-4 control-label">Confirmez votre mot de passe</label>

                  <div class="col-md-6">
                    @foreach($users as $client )
                    <input type="password" class="form-control" name="password_confirmation" value="{{$client->password}}" required>
                    @endforeach
                    @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-4 control-label">Droits</label>
                  <div class="col-md-6">
                    @foreach($users as $client)
                    @if(auth()->user()->admin === 2)
                    {{ Form::select('admin', [ 'Client', 'Administrateur', 'SuperAdministrateur'],null, ['class' => 'form-control'])}}
                    @elseif(auth()->user()->admin === 1)
                    {{ Form::select('admin', [ 'Client'],null, ['class' => 'form-control'])}}
                    @endif
                    @endforeach
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-success" style="">
                      <i class="fa fa-btn fa-user" style="margin-right: 0.5em;"></i>Modifier le compte
                    </button>
                    <a type="button" class="btn btn-default" href="{{url('/home')}}" style="margin-left: 7em;">Annuler</a>
                  </div>
                </div>
              </div>
              <div id="parametresadmin" class="tab-pane fade">
                <div class="form-group">
                  <label for="messages_sms">Message SMS</label>
                  @foreach($users as $client)
                  <textarea class="form-control" rows="8" name="messages_sms" id="messages_sms" value="{{$client->messages_sms}}">{{$client->messages_sms}}</textarea>
                  @endforeach
                </div>
                <div class="form-group">
                  <label for="messages_mail">Message mail</label>
                  @foreach($users as $client)
                  <textarea class="form-control" rows="8" id="messages_mail" name="messages_mail" value="{{$client->messages_mail}}">{{$client->messages_mail}}</textarea>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection
