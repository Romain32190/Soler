<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicules extends Model
{
    
    protected $table = 'vehicules';


    protected $fillable = ['id_client', 'id_admin', 'immatriculation', 'num_serie', 'type_vehicule', 'num_moteur', 'kilometre_reel', 'nombre_heure', 'description', 'nom', 'id_vehicule'];


    //Jointure user//

      public function user(){
        
        return $this->belongsTo('App\User', 'id_client', 'id');

      }

    //Jointure admin//

      public function admin(){
        return $this->hasMany('App\User', 'id_admin', 'id');
      }


    //Jointure Interventions//
     public function recupinterventions(){
         return $this->hasMany('App\Interventions', 'id_vehicule', 'id');
     }
}
