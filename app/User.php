<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Vehicules;

class User extends Authenticatable
{
    
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'email','tel', 'password', 'admin', 'id_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

      //Jointure vehicule//

      public function vehicules(){
          return $this->hasMany('App\Vehicules', 'id_client', 'id');
      }


      public function adminVehicules(){
          return $this->belongsTo('App\Vehicules', 'id_admin', 'id');
      }

      public function adminType(){
          return $this->belongsTo('App\TypeInter', 'id_admin', 'id');
      }


      public function adminInterventions(){
          return $this->belongsTo('App\Interventions', 'id_admin', 'id');
      }

      public function userinter(){
          return $this->hasMany('App\Interventions', 'id_client', 'id');
      }

      //Role Super Administrateur//
      
      public function isAdmin(){
          return $this->admin;
      }

      //Role Administrateur//

      public function admin(){
          return $this->admin;
      }


}
