<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeInter extends Model
{
    public $table = 'type_controles';

    protected $fillable = ['code', 'nom', 'periodicite', 'unite', 'alerte', 'id_admin'];

    public function type(){
    	return $this->hasMany('App\User', 'id_admin', 'id');
    }

    public function interventionstype(){
    	return $this->hasMany('App\Interventions', 'id_controle', 'id');
    }

    public function recupcontroletype(){
    	return $this->hasMany('App\Controle', 'id_controle', 'id');
    }
    

}
