<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Interventions extends Model
{
   protected $table = 'interventions';


   protected $fillable = ['description','id_controle', 'id_admin', 'id_client', 'id_vehicule', 'type_controle', 'kilometre_reel', 'dernier_controle_km', 'prochain_controle_km', 'nombre_heure','date_realisation'];

   public function interventionsadmin(){
   	 return $this->hasMany('App\User', 'id_admin', 'id');
   }

   public function recupvehicules(){
       return $this->belongsTo('App\Vehicules', 'id_vehicule', 'id');
   }

   public function recupuser(){
       return $this->belongsTo('App\User', 'id_client', 'id');
   }

   public function recupcontrole(){
       return $this->belongsTo('App\TypeInter', 'id_controle', 'id');
   }


   public function recupinfoscontroles(){
       return $this->belongsTo('App\Controle', 'id_vehicule', 'id');
   }

}
