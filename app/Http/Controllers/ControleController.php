<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicules;
use App\TypeInter;
use App\Controle;
use App\Interventions;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class ControleController extends Controller
{
    public function index(){
      $vehicule = Vehicules::where('id', $id)->get();
      $controle = Controle::where('id', '=', $id)->get();

      return view('editionvehicule', ['vehicules' => $vehicule, 'type' => TypeInter::all(), 'interventions' => Interventions::all(), 'client' => User::all(), 'controle' => Controle::all()]);
    }

    // Validation et insertion controle //

    public function store($id, Request $request) {

        $derniercontrole = Input::get('dernier_controle');
        $idcontrole = Input::get('id_controle');
        $derniercontroledate = strtotime($derniercontrole);
        $recuperation = TypeInter::select('periodicite', 'unite')->where('id', '=', $idcontrole)->get();



        foreach ($recuperation as $result) {
                $maperiodicite = $result->periodicite;
        }

        foreach ($recuperation as $unite) {
                $uniterecup = $unite->unite;
        }
            
        $resultunite = $uniterecup;

        if($uniterecup == 'km' ){

        $prochaincontrole = $derniercontrole + $maperiodicite;

        }elseif($uniterecup == 'heures'){

        $prochaincontrole = $derniercontrole + $maperiodicite;

        }elseif($uniterecup == 'mois'){

        $prochaincontroledate = $derniercontroledate + $maperiodicite;
        }




         $controle = new Controle();

        if($uniterecup == 'km'){
        $controle->dernier_controle = $derniercontrole;
        $controle->prochain_controle = $prochaincontrole;
        $controle->id_controle = $request->id_controle;
        $controle->id_admin = $request->id_admin;
        $controle->id_client = $request->id_client;
        $controle->id_vehicule = $request->id_vehicule;

        $controle->save();
      }

        elseif($uniterecup == 'heures'){
        $controle->dernier_controle = $derniercontrole;
        $controle->prochain_controle = $prochaincontrole;
        $controle->id_controle = $request->id_controle;
        $controle->id_admin = $request->id_admin;
        $controle->id_client = $request->id_client;
        $controle->id_vehicule = $request->id_vehicule;

        $controle->save();
      }

        elseif($uniterecup == 'mois'){
        $controle->dernier_controle = $derniercontroledate;
        $controle->prochain_controle = $prochaincontroledate;
        $controle->id_controle = $request->id_controle;
        $controle->id_admin = $request->id_admin;
        $controle->id_client = $request->id_client;
        $controle->id_vehicule = $request->id_vehicule;

        $controle->save();
      }



       $intervention = new Interventions();

       if($uniterecup == 'km') {
       $intervention->dernier_controle_km = $derniercontrole;
       $intervention->prochain_controle_km = $prochaincontrole;

       }

       if($uniterecup == 'heures') {
       $intervention->dernier_controle_heure = $derniercontrole;
       $intervention->prochain_controle_heure = $prochaincontrole;
       }

       if($uniterecup == 'mois'){
       $intervention->dernier_controle_date = $request->dernier_controle;
       $intervention->prochain_controle_date = date('Y-m-d', strtotime('+'.$maperiodicite.'month', $derniercontroledate));
       }
       $intervention->id_controle = $request->id_controle;
       $intervention->id_client = $request->id_client;
       $intervention->id_admin = $request->id_admin;
       $intervention->id_vehicule = $request->id_vehicule;

       $intervention->save();
        
        return response()->json($this);
        return redirect('/home');
    }


    public function delete($id) {
    }
}
