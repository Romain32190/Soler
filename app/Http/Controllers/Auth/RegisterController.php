<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Vehicules;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ItemRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
       $this->middleware('admin');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'login' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'admin' => 'required',
            'id_admin' => 'required',
            'tel' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array  $data)
    {

    // echo('<pre>');
    //     var_dump($data);
    //     die();
    // echo('</pre>');

       

        return User::create([
            'name' => $data['name'],
            'login' => $data['login'],
            'email' => $data['email'],
            'tel' => $data['tel'],
            'admin' => $data['admin'],
            'id_admin' => $data['id_admin'],
            'password' => bcrypt($data['password']),
        ]);
        

        return redirect('/home');
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
           $this->throwValidationException(
               $request, $validator
           );
        }

        $this->create($request->all());

        return redirect('/login');

    }



    public function show($id)
    {
        $users = User::where('id', $id)->get(); 
        return view('modifregister', ['users' => $users]);

    }

    public function update($id, Request $request){
          $name = $request->input('name');
          $tel = $request->input('tel');
          $admin = $request->input('admin');
          $id_admin = $request->input('id_admin');
          $password = Hash::make($request->input('password'));
          $messagesms = $request->input('messages_sms');
          $messagemail = $request->input('messages_mail');
          User::where('id', $id)->update(['name' => $name, 'password' => $password, 'admin' => $admin, 'tel' => $tel, 'messages_sms' => $messagesms, 'messages_mail' => $messagemail, 'id_admin' => $id_admin]);

          return redirect('/home');
    }


}
