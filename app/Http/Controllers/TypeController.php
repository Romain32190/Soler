<?php

namespace App\Http\Controllers;

use App\TypeInter;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $type = TypeInter::all();

    }

    public function store(Request $request)

    {

      
        

      $this->validate(request(), [

        'code' => 'required',
        'nom' => 'required',
        'periodicite' => 'required',
        'unite' => 'required',
        'alerte' => 'required',
        'id_admin' => 'required',
      ]);

      TypeInter::create(request()->all());


      return response()->json($this);
      return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
