<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vehicules;
use App\TypeInter;
use App\Interventions;

class AdminController extends Controller
{

	public function __construct()

  	{
    	$this->middleware('admin');
 	}


    // Controllers admin qui voit que ses clients //

	public function pageadmin() {

    $vehicules = Vehicules::all();
    $recupuser = User::all();
    $typeinter = TypeInter::all();
    $interventions = Interventions::all();

    return view('admin',

    [
         'vehicules' => Vehicules::where('id_admin', '=', Auth::id())
         ->orWhere('id_client', '=', Auth::id())->get(),

         'client' => User::where('id_admin','=', Auth::id())->get(), 

         'type' =>TypeInter::where('id_admin', '=' , Auth::id())->get(), 

         'interventions' => Interventions::where('id_admin', '=', Auth::id())
         ->orWhere('id_client', '=', Auth::id())->get(),

         'controle' => \App\Controle::where('id_admin', '=' , Auth::id())
         ->orWhere('id_client', '=', Auth::id())->get()

    ]);
  }


  public function filteradmin(){
    
     $nom = \Request::get('controle');
      $types = [];

      if(isset($nom)){
         $types = Interventions::where('id_controle','like','%'.$nom.'%')
         ->orderBy('id_controle')
         ->paginate(); 
      }

      //Filtres recherche client//

      $name = \Request::get('client');

      $clients = [];

      if(isset($name)){
         $clients = User::where('id', 'like', '%'.$name.'%')
         ->orderBy('id')
         ->paginate();
      }


      $user = \Request::get('clientecheancesadmin');

      $users = [];

      if(isset($user)){
         $users = Interventions::where('id_client', 'like', '%'.$user.'%')
         ->orderBy('id_client')
         ->paginate();
      }


      //Filtres liste deroulante type de vehicule//

      $vehicule = \Request::get('type_vehicule');


      $typevehicule = [];
      if(isset($vehicule)){
         $typevehicule = Interventions::where('id_vehicule', 'like', '%'.$vehicule.'%')
         ->orderBy('id_vehicule')
         ->paginate();
      }

      $immatriculationvehicule = \Request::get('immatriculationvehicule');


      $vehiculeimmac = [];
      if(isset($immatriculationvehicule)){
         $vehiculeimmac = Vehicules::where('id', 'like', '%'.$immatriculationvehicule.'%')
         ->orderBy('id')
         ->paginate();
      }


      //Filtres de recherche de l'immatriculation//

      $immatriculation = \Request::get('immatriculationecheances');
      $plaque=[];
      if(isset($immatriculation)){
         $plaque = Interventions::where('id_vehicule', 'like', '%'.$immatriculation.'%')
         ->orderBy('id_vehicule')
         ->paginate();
      }


      $rechercheclientvehicule = \Request::get('clientvehicule');
      $clientvehicule=[];
      if(isset($rechercheclientvehicule)){
         $clientvehicule = Vehicules::where('id_client', 'like', '%'
         .$rechercheclientvehicule.'%')
         ->orderBy('id_client')
         ->paginate();
      }


      $clientinter = \Request::get('clientinter');
      $clientintervention=[];
      if(isset($clientinter)){
         $clientintervention = Interventions::where('id_client', 'like', '%'.$clientinter.'%')
         ->orderBy('id_client')
         ->paginate();
      }


      $typeinter = \Request::get('typeinter');
      $intertype=[];
      if(isset($typeinter)){
         $intertype = Interventions::where('id_vehicule', 'like', '%'.$typeinter.'%')
         ->orderBy('id_vehicule')
         ->paginate();
      }

      $immacinter = \Request::get('immacinter');
      $interimmac=[];
      if(isset($immacinter)){
         $interimmac = Interventions::where('id_vehicule', 'like', '%'.$immacinter.'%')
         ->orderBy('id_vehicule')
         ->paginate();
      }


      $controleinter = \Request::get('controleinter');
      $intercontrole=[];
      if(isset($controleinter)){
         $intercontrole = Interventions::where('id_controle', 'like', '%'.$controleinter.'%')
         ->orderBy('id_controle')
         ->paginate();
      }

      $resultvehicule = [];

      for($i=0; $i < count($typevehicule);$i++) {
         array_push($resultvehicule, $typevehicule[$i]);
      }
      for($j=0; $j < count($plaque);$j++) {
         array_push($resultvehicule, $plaque[$j]);
      }

      for ($s=0; $s < count($clients) ; $s++) {
         array_push($resultvehicule, $clients[$s]);
      }

      for ($t=0; $t < count($types); $t++) {
         array_push($resultvehicule, $types[$t]);
      }

      for ($m=0; $m < count($users) ; $m++) {
         array_push($resultvehicule, $users[$m]);
      }

      for ($a=0; $a < count($vehiculeimmac); $a++) { 
         array_push($resultvehicule, $vehiculeimmac[$a]);
      }

      for ($c=0; $c < count($clientvehicule); $c++) { 
         array_push($resultvehicule, $clientvehicule[$c]);
      }

      for ($x=0; $x < count($clientintervention); $x++) { 
         array_push($resultvehicule, $clientintervention[$x]);
      }

      for ($w=0; $w < count($intertype); $w++) { 
         array_push($resultvehicule, $intertype[$w]);
      }

      for ($q=0; $q < count($interimmac) ; $q++) { 
         array_push($resultvehicule, $interimmac[$q]);
      }

      for ($d=0; $d < count($intercontrole); $d++) { 
         array_push($resultvehicule, $intercontrole[$d]);
      }

      return view('admin', [
         'vehicules' => $resultvehicule, 
         'type' => $resultvehicule, 
         'client' => $resultvehicule, 
         'interventions' => $resultvehicule]);
   }
}
