<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vehicules;
use App\TypeInter;
use App\Interventions;
use App\Controle;
use Excel;
use Illuminate\Support\Facades\Input;
use DateTime;
use Datatables;

class VehiculeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Recuperation//


    public function index()
    {
        $vehicules = Vehicules::all();
        $recupuser = User::all();
        return view('administration', ['vehicules' => Vehicules::all(), 'client' => User::  all(), 'interventions' => Interventions::all(), 'type' => TypeInter::all(), 'controle' => Controle::all()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //Stockage base de données//

    public function store(Request $request)
    
    {

        $this->validate(request(), [
            'id_client' => 'required',
            'immatriculation' => 'required',
            'num_serie' => 'nullable|string',
            'type_vehicule' => 'required',
            'num_moteur' => 'nullable|string',
            'nombre_heure' => 'nullable|string',
            'kilometre_reel' => 'nullable|string',
            'description' => 'nullable|string',
            'nom' => 'required',
            'id_admin' => 'required',

        ]);

        Vehicules::create(request()->all());

        $idvehicule = DB::getPdo()->lastInsertId();
        $derniercontrole = Input::get('dernier_controle');
        $idcontrole = Input::get('id_controle');
        $derniercontroledate = strtotime($derniercontrole);
        $recuperation = TypeInter::select('periodicite', 'unite')->where('id', '=', $idcontrole)->get();


        foreach ($recuperation as $result) {
                $maperiodicite = $result->periodicite;
        }

        foreach ($recuperation as $unite) {
                $uniterecup = $unite->unite;
        }
            
        $resultunite = $uniterecup; 
        
        if($uniterecup == 'km' ){

        $prochaincontrole = $derniercontrole + $maperiodicite;

        }elseif($uniterecup == 'heures'){

        $prochaincontrole = $derniercontrole + $maperiodicite;

        }elseif($uniterecup == 'mois'){

        $prochaincontroledate = $derniercontroledate + $maperiodicite;
        }



        $controle = new Controle();

        if($uniterecup == 'km'){
        $controle->dernier_controle = $derniercontrole;
        $controle->prochain_controle = $prochaincontrole;
        $controle->id_controle = $request->id_controle;
        $controle->id_admin = $request->id_admin;
        $controle->id_client = $request->id_client;
        $controle->id_vehicule = $idvehicule;

        $controle->save();
      }

        elseif($uniterecup == 'heures'){
        $controle->dernier_controle = $derniercontrole;
        $controle->prochain_controle = $prochaincontrole;
        $controle->id_controle = $request->id_controle;
        $controle->id_admin = $request->id_admin;
        $controle->id_client = $request->id_client;
        $controle->id_vehicule = $idvehicule;

        $controle->save();
      }

        elseif($uniterecup == 'mois'){
        $controle->dernier_controle = $derniercontroledate;
        $controle->prochain_controle = $prochaincontroledate;
        $controle->id_controle = $request->id_controle;
        $controle->id_admin = $request->id_admin;
        $controle->id_client = $request->id_client;
        $controle->id_vehicule = $idvehicule;

        $controle->save();
      }

       $intervention = new Interventions();

       $intervention->id_controle = $request->id_controle;
       $intervention->id_client = $request->id_client;
       $intervention->id_admin = $request->id_admin;
       $intervention->id_vehicule = $idvehicule;

       if($uniterecup == 'km') {
       $intervention->dernier_controle_km = $derniercontrole;
       $intervention->prochain_controle_km = $prochaincontrole;
       }
       if($uniterecup == 'heures') {
       $intervention->dernier_controle_heure = $derniercontrole;
       $intervention->prochain_controle_heure = $prochaincontrole;
       }
       if($uniterecup == 'mois'){
       $intervention->dernier_controle_date = $request->dernier_controle;
       $intervention->prochain_controle_date = date('Y-m-d', strtotime('+'.$maperiodicite.'month', $derniercontroledate));
       }

       $intervention->save();

        return response()->json($this);


        return redirect('/home');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $modifkm = Vehicules::where('id', $id)->get();
        $controle = Controle::where('id', $id)->get();
    }


    public function showmodificationvehicule($id){

        $modifvehicule = Vehicules::where('id', $id)->get();
        $controle = Controle::where('id_vehicule', $id)->get();
        $client = User::where('id', $id)->get();
        $interventions = Interventions::where('id_vehicule', $id)->get();
        return view('editionvehicule', ['vehicules' => $modifvehicule, 'type' => TypeInter::all(), 'interventions' => $interventions, 'client' => $client, 'controle' => $controle]);
    }


    public function updatevehicule($id, Request $request)

    {


          // Vehicules update //
          $immatriculation = $request->input('immatriculation');
          $num_serie = $request->input('num_serie');
          $type_vehicule = $request->input('type_vehicule');
          $nom = $request->input('nom');
          $num_moteur = $request->input('num_moteur');
          $description = $request->input('description');
          $nombre_heure = $request->input('nombre_heure');
          $kilometre_reel = $request->input('kilometre_reel');


          Vehicules::where('id', $id)->update([
          'immatriculation' => $immatriculation, 
          'num_serie' => $num_serie, 
          'nom' => $nom,
          'type_vehicule' => $type_vehicule, 
          'num_moteur' => $num_moteur, 
          'description' => $description, 
          'nombre_heure' => $nombre_heure, 
          'kilometre_reel' => $kilometre_reel,

          ]);
            
        return redirect('/home');
        return response()->json($this);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Modif kilometres vehicules //

    public function update($id, Request $request)
    {
        $kilometre_reel = $request->input('kilometre_reel');
        Vehicules::where('id', $id)->update(['kilometre_reel' => $kilometre_reel]);

        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $controle = Controle::findOrFail($id);
       $controle->delete();

      return back();
    }

      public function importExport()
  {
    return view('administration');
  }

  public function downloadExcel($type)
  {
    $data = [];
    return Excel::create('vehicules.export', function($excel) use ($data) {
      $excel->sheet('mySheet', function($sheet) use ($data) {
          $vehicules = Vehicules::all();
                foreach($vehicules as $vehicule) {
                 $data[] = array(
                      $vehicule->user['name'],
                      $vehicule->type_vehicule,
                      $vehicule->nom,
                      $vehicule->immatriculation,
                      $vehicule->kilometre_reel,
                      $vehicule->nombre_heure,


            
                    
                );
            }
      
    $sheet->fromArray($data, null, 'A1', false, false);
   $headings = array('Client','Type' ,'Véhicule' ,'Immatriculation', 'Km reel', 'Heure reel');
    $sheet->prependRow(1, $headings);
        });
    })->export('xls');
  }
}
