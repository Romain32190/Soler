<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interventions;
use App\Vehicules;
use App\User;
use App\Controle;
use App\TypeInter;
use DateTime;
use Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class InterventionsController extends Controller
{
  public function index()

  {
    $vehicules = Vehicules::all();
    $recupuser = User::all();
    $interventions = Interventions::all();
    return view('administration', [

      'vehicules' => Vehicules::all(), 
      'client' => User::all(), 
      'interventions' => Interventions::all(),
      'controle' => Controle::all()]);

  }

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{

}

/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */

//Stockage base de données//

public function store($id, Request $request)
{

  $description = $request->input('description');
  $idcontrole = Input::get('id_controle');
  $idvehicule = Input::get('id_vehicule');
  $derniercontrole = Input::get('dernier_controle');
  //$derniercontroledate = strtotime($derniercontrole);
  
  //Calcul kilomètre + recherche unité insertion base//

  $prochaincontroleheure = Input::get('nombre_heure_inter');
  $prochaincontrole = Input::get('kilometre_reel_inter');
  $prochaincontrolerecupdate = Input::get('prochain_controle');
  $prochaincontroledate = strtotime($prochaincontrolerecupdate);

  $calculinter = TypeInter::select('periodicite')->where('id', '=', $idcontrole)->get();

  $uniteinter = TypeInter::select('unite')->where('id', '=', $idcontrole)->get();



  foreach ($uniteinter as $unitekm) {
    $uniterecupkm = $unitekm->unite;
  }

  $resultunite = $uniterecupkm; 

  foreach ($calculinter as $result) {

    $periodicite = $result->periodicite;
  }

  $calcul = $prochaincontrole + $periodicite;
  $calculheure = $prochaincontroleheure + $periodicite;
  $calculdate = $prochaincontroledate + $periodicite;

    $intervention = new Interventions();

    if($resultunite == 'km'){
      $intervention->dernier_controle_km = $request->kilometre_reel_inter;
      $intervention->prochain_controle_km = $calcul;
    }
    if($resultunite == 'heures'){

      $intervention->dernier_controle_heure = $request->nombre_heure_inter;
      $intervention->prochain_controle_heure = $calculheure;
    }
    if($resultunite == 'mois'){ 
      $intervention->dernier_controle_date = $request->prochain_controle;
      $intervention->prochain_controle_date = date('Y-m-d', strtotime('+'.$periodicite.'month', $prochaincontroledate));
    }
    $intervention->description = $description; 
    $intervention->id_controle = $request->id_controle;
    $intervention->id_client = $request->id_client;
    $intervention->id_admin = $request->id_admin;
    $intervention->id_vehicule = $idvehicule;

    $intervention->save();


  if($resultunite == 'km'){
    Interventions::where('id', $id)->update([ 
      'dernier_controle_km' => $derniercontrole,
      'prochain_controle_km' => $request->get('kilometre_reel_inter'),
      'description' => $description,
      'date_realisation' => new DateTime(),
      'created_at' => new DateTime(),
      'updated_at' => new DateTime(),
    ]);
  }

  elseif($resultunite == 'heures'){
      Interventions::where('id', $id)->update([ 
        'dernier_controle_heure' => $derniercontrole,
        'prochain_controle_heure' => $request->get('nombre_heure_inter') ,
        'description' => $description,
        'date_realisation' => new DateTime(),
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
      ]);    
  }
  elseif($resultunite == 'mois'){
    Interventions::where('id', $id)->update([ 
        'dernier_controle_date' => $derniercontrole,
        'prochain_controle_date' => $request->get('prochain_controle'),
        'description' => $description,
        'date_realisation' => new DateTime(),
        'created_at' => new DateTime(),
        'updated_at' => new DateTime(),
      ]); 
  }


    if($resultunite == 'km'){

    Vehicules::where('id', $idvehicule)->update([
     'kilometre_reel' => $request->get('kilometre_reel_inter'),
     'created_at' => new DateTime(),
     'updated_at' => new DateTime(),
   ]);
    
    }

    elseif($resultunite == 'heures'){
     Vehicules::where('id', $idvehicule)->update([
     'nombre_heure' => $request->get('nombre_heure_inter'),
     'created_at' => new DateTime(),
     'updated_at' => new DateTime(),
   ]);
}





  if($resultunite == 'km'){

   Controle::where('id', $idvehicule)->update([
    'dernier_controle' => $request->get('kilometre_reel_inter'), 
    'prochain_controle' => $calcul,
    'created_at' => new DateTime(),
    'updated_at' => new DateTime(),

  ]);

 }

 elseif($resultunite == 'heures'){
  Controle::where('id', $idvehicule)->update([
    'dernier_controle' => $request->get('nombre_heure_inter'),
    'prochain_controle' => $calculheure,
    'created_at' => new DateTime(),
    'updated_at' => new DateTime(),

  ]);
}

elseif($resultunite == 'mois'){
  Controle::where('id', $idvehicule)->update([
    'dernier_controle' => $prochaincontroledate,
    'prochain_controle' =>  $calculdate,
    'created_at' => new DateTime(),
    'updated_at' => new DateTime(),
  ]);
}


return redirect('/home');

}





/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
  $vehicules = Vehicules::where('id', $id)->get();
  $type = \App\TypeInter::where('id', $id)->get();
  $interventions = Interventions::where('id', $id)->get();
  $client = User::where('id', $id)->get();
  $controle = Controle::where('id', $id)->get();

  return view('validationinterventions', [
    'vehicules' => $vehicules, 
    'type' => $type, 
    'interventions' => $interventions, 
    'client' => $client, 
    'controle' => $controle]);



}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    //
}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{

}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    //
}

public function importExport()
{
  return view('administration');
}

public function downloadExcel($type)
{
  $data = [];
  return Excel::create('interventions.export', function($excel) use ($data) {
    $excel->sheet('mySheet', function($sheet) use ($data) {
      $interventions = Interventions::all();
      foreach($interventions as $intervention) {
       $data[] = array(
        $intervention->recupuser['name'],
        $intervention->recupvehicules['type_vehicule'],
        $intervention->recupvehicules['nom'],
        $intervention->recupvehicules['immatriculation'],
        $intervention->recupcontrole['nom'],
        $intervention->recupvehicules['kilometre_reel'],
        $intervention->recupvehicules['nombre_heure'],
        $intervention->recupinfoscontroles['prochain_controle'],
        $intervention->prochain_controle_km - $intervention->recupvehicules['kilometre_reel'],
        $intervention->prochain_controle_heure - $intervention->recupvehicules['nombre_heure'],



      );
     }

     $sheet->fromArray($data, null, 'A1', false, false);
     $headings = array('Client','Type' ,'Véhicule' ,'Immatriculation','Type d\'inter', 'Km reel', 'Heure reel','Prochain contrôle', 'Controle en km dans ...', 'Controle en heure dans ...');
     $sheet->prependRow(1, $headings);
   });
  })->export('xls');
}
}
