<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vehicules;
use App\TypeInter;
use App\Interventions;
use App\Controle;

class ClientController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */

  // Vue client qui voit ses echeances, ses interventions et ses vehicules //
  
  public function client()

  {  
    $vehicules = Vehicules::all();
    $recupuser = User::all();
    $typeinter = TypeInter::all();
    $interventions = Interventions::all();
      return view('clients', ['vehicules' => Vehicules::all(), 'client' => User::all(), 'type' => TypeInter::all(),'interventions' => Interventions::all(), 'controle' => Controle::all()]);
  }



  // Clients filtres //


  public function clientfiltres() 

  {
    $vehicule = \Request::get('type_vehicule');

    $typevehicule = [];

    if(isset($vehicule)) {
    $typevehicule = Vehicules::where('type_vehicule','like','%'.$vehicule.'%')
      ->where('id_client', Auth()->id())
      ->orderBy('type_vehicule')
      ->paginate();
    }

    $immatriculation = \Request::get('immatriculationclient');

    $plaque = [];

    if(isset($immatriculation)) {
      $plaque = Interventions::where('id_vehicule', 'like', '%'.$immatriculation.'%')
      ->orderBy('id_vehicule')
      ->paginate();
    }

    $resultvehicule = [];

    for($t=0; $t < count($typevehicule); $t++) {
      array_push($resultvehicule, $typevehicule[$t]);
    }

    for($p=0; $p < count($plaque); $p++) {
      array_push($resultvehicule, $plaque[$p]);
    }

    return view('clients', [
      'vehicules' => $resultvehicule, 
      'type' => $resultvehicule, 
      'client' => $resultvehicule, 
      'interventions' => $resultvehicule, 
      'controle' => $resultvehicule]);
  }

}
