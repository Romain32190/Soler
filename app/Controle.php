<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Controle extends Model
{
    protected $table = 'controles';

    public $incrementing = false;


    protected $fillable = ['id_controle','dernier_controle', 'id_vehicule', 'id_admin', 'id_client', 'prochain_controle'];

     public function recupvehicules(){
         return $this->belongsTo('App\Vehicules', 'id_vehicule', 'id');
    }

    public function controleadmin(){
        return $this->hasMany('App\User', 'id_admin', 'id');
    }

    public function intercontrole(){
        return $this->hasMany('App\Interventions', 'id_vehicule', 'id');
    }


    public function recuptypeinter(){
        return $this->belongsTo('App\TypeInter', 'id_controle', 'id');
    }
}
