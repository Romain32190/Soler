<?php

use Illuminate\Database\Seeder;
use App\Vehicules;

class VehiculesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Vehicules::truncate();
      $faker = Faker\Factory::create('fr_FR');
      $limit = 5;
      for ($i = 0; $i < $limit; $i++) {
          Vehicules::create([
              'id_client' => $i+1,
              'immatriculation' => $faker->swiftBicNumber,
              'num_serie' => $faker->isbn13,
              'nom' => 'Camion 3T',
              'type_vehicule' => 'VU',
              'num_moteur' => $faker->isbn13,
              'description' => 'Bonjour',
              'actif' => '1',
              'kilometre_reel' => $faker->randomNumber(6),
              'nombre_heure' => $faker->randomNumber(4),
          ]);
        }
    }
}
