<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::truncate();
      $faker = Faker\Factory::create('fr_FR');
      $limit = 5;
      for ($i = 0; $i < $limit; $i++) {
          User::create([
              'name' => $faker->name,
              'login' => $faker->userName,
              'password' => $faker->password,
              'remember_token' => bcrypt(('password')),
              'email' => $faker->email,
              'tel' => $faker->phoneNumber,
              'messages_sms' => 'Bonjour',
              'messages_mail' => 'Aurevoir',
          ]);
        }
    }
}
