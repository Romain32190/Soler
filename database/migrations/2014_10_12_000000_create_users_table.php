<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('id_admin');
            $table->string('name');
            $table->string('login')->unique();
            $table->string('email')->unique();
            $table->integer('tel');
            $table->boolean('actif');
            $table->string('password');
            $table->boolean('admin')->default(false);
            $table->string('messages_sms');
            $table->string('messages_mail');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
