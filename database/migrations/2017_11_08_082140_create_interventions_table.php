<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterventionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interventions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_controle');
            $table->integer('id_vehicule');
            $table->integer('id_client');
            $table->integer('id_admin');
            $table->integer('dernier_controle_km');
            $table->integer('prochain_controle_km');
            $table->integer('dernier_controle_heure');
            $table->integer('prochain_controle_heure');
            $table->date('dernier_controle_date');
            $table->date('prochain_controle_date');
            $table->string('description');
            $table->date('date_realisation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interventions');
    }
}
