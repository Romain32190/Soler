<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_client');
            $table->integer('id_admin');
            $table->string('immatriculation');
            $table->string('nom');
            $table->string('num_serie');
            $table->string('num_moteur');
            $table->string('type_vehicule');
            $table->string('description');
            $table->boolean('actif');
            $table->float('kilometre_reel');
            $table->float('nombre_heure');
            $table->timestamps();
        });
    }

     public function down()
    {
        Schema::dropIfExists('vehicules');
    }
}
