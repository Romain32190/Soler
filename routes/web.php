<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
use App\Vehicules;
use App\TypeInter;
use App\Controle;
use App\Interventions;



//Route connexion Utilisateur//

Route::get('/', function () {
    return view('auth.login');
});

//Route de l'authentification//
Auth::routes();

//Route principal//
Route::get('/home', 'HomeController@index')->name('home');

//Route get pour recuperer l'id de l'utilisateur//
Route::get('/modifregister{id}', 'Auth\RegisterController@show' );

//Route post pour valider la modification des champs//
Route::post('/home/modifregister{id}', 'Auth\RegisterController@update');

//Route get pour recuperer l'id du vehicule//
Route::get('/modifkm{id}', 'VehiculeController@show');

//Route post pour valider la modification du km partie vehicules//
Route::post('/home/modificationkm{id}', 'VehiculeController@update');


//Route plateforme Admin///
Route::get('/admin', ['middleware' => 'admin', 'uses' => 'AdminController@pageadmin']);


//Route plateforme SuperAdmin
Route::get('/home', ['middleware' => 'superadmin', 'uses' => 'HomeController@index']);


//Route plateforme client//

Route::get('/tableaudebord', 'ClientController@client');

//Formulaire typecontrole//
Route::resource('/home/typecontrole', 'TypeController');

//Formulaire Vehicules//
Route::resource('/home/vehicule', 'VehiculeController');

//Route id bouton interventions//
Route::get('/interventions{id}', 'InterventionsController@show');

//Route formulaire interventions//
Route::post('/validerinterventions{id}', 'InterventionsController@store');

//Route get pour les filtres//
Route::get('/home/filtres', 'HomeController@filtres');

Route::get('/modifvehicule{id}', 'VehiculeController@showmodificationvehicule');

Route::post('/home/modificationvehicule{id}', 'VehiculeController@updatevehicule');

Route::get('/tableaudebord/clientfiltres', 'ClientController@clientfiltres');

Route::get('/admin/adminfiltres', 'AdminController@filteradmin');

Route::post('/home/nouveaucontrole{id}', 'ControleController@store');

Route::delete('/suppressioncontrole{id}', 'VehiculeController@destroy');

Route::get('importExport', 'InterventionsController@importExport');

Route::get('downloadExcel/{type}', 'InterventionsController@downloadExcel');

Route::post('importExcel', 'InterventionsController@importExcel');

Route::get('importExport', 'VehiculeController@importExport');

Route::get('downloadExcelvehicule/{type}', 'VehiculeController@downloadExcel');

Route::post('importExcel', 'VehiculeController@importExcel');




//Route de test//

Route::get('test', function() {

$machin = App\Controle::find(3);
return response()->json($machin->recuptypedecontroleinterventions);

});

