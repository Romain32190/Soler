$(function() {
  var lastTab = localStorage.getItem('lastTab');
  $('.onglet, .tab-content').removeClass('hidden');
  if (lastTab) {
    $('[data-target="' + lastTab + '"]').tab('show');
  }
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    localStorage.setItem('lastTab', $(this).data('target'));
  });
 
  $('#nouveauvehicule').click(function() {
    $('#vehicule').modal();
  });

  $(document).on('submit', '#formvehicule', function(e) {  
    e.preventDefault();
     
    $('input+span').text('');
    $('input').parent().removeClass('has-error');
     
    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: "json"
    })
    .done(function(data) {
   		$('.alert-success').removeClass('hidden');
        $('#vehicule').modal('hide');
        setTimeout(function(){window.location.reload(true);}, 1000);

    })
    .fail(function(data) {
        $.each(data.responseJSON, function (key, value) {
         var input = '#formvehicule input[name=' + key + ']';
         $(input + '+span').text(value);
         $(input).parent().addClass('has-error')

         console.log(data);
       });
    });
 });

  $('#nouveautype').click(function() {
    $('#type').modal();
  });


  $(document).on('submit', '#formtype', function(e) {  
    e.preventDefault();
     
    $('input+span').text('');
    $('input').parent().removeClass('has-error');
     
    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: "json"
    })
    .done(function(data) {
   		$('.alert-success').removeClass('hidden');
        $('#type').modal('hide');
        setTimeout(function(){window.location.reload(true);}, 1000);

    })
    .fail(function(data) {
        $.each(data.responseJSON, function (key, value) {
         var input = '#formtype input[name=' + key + ']';
         $(input + '+span').text(value);
         $(input).parent().addClass('has-error')
         console.log(data);
      });
    });
  });


   $('#nouveaucontrole').click(function() {
   $('#controle').modal();
  });
   
   $(document).on('submit', '#formcontrole', function(e) {  
    e.preventDefault();
     
    $('input+span').text('');
    $('input').parent().removeClass('has-error');
     
    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: "json"
    })
    .done(function(data) {
      $('.alert-success').removeClass('hidden');
        $('#controle').modal('hide');
        setTimeout(function(){window.location.reload(true);}, 1000);

    })
    .fail(function(data) {
        $.each(data.responseJSON, function (key, value) {
         var input = '#formcontrole input[name=' + key + ']';
         $(input + '+span').text(value);
         $(input).parent().addClass('has-error')
         console.log(data);
      });
    });
  });


     $('#nouveaucontrole').click(function() {
   $('#controle').modal();
  });
   
   $(document).on('submit', '#formintervention', function(e) {  
    e.preventDefault();
     
    $('input+span').text('');
    $('input').parent().removeClass('has-error');
     
    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: "json"
    })
    .done(function(data) {
      $('.alert-success').removeClass('hidden');
        $('#controle').modal('hide');
        setTimeout(function(){window.location.reload(true);}, 1000);

    })
    .fail(function(data) {
        $.each(data.responseJSON, function (key, value) {
         var input = '#formintervention input[name=' + key + ']';
         $(input + '+span').text(value);
         $(input).parent().addClass('has-error')
         console.log(data);
      });
    });
  });

   
});
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable2");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc"; 
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++; 
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
